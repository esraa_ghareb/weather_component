import React, { Component } from "react";

import { TiWeatherPartlySunny } from "react-icons/ti";
import { GiModernCity } from "react-icons/gi";
import { BiTime } from "react-icons/bi";
class App extends Component {
  constructor() {
    super();
    this.Api =
      "https://api.openweathermap.org/data/2.5/forecast?q=Cairo&units=metric&appid=2acfc1b8fbf8d39941504a3f2e405a09";
    this.state = {
      cityName: null,
      daysWeather: [
        {
          dayName: null,
          dayDate: null,
          weatherIcon: "01n",
          weatherTemp: null,
          weatherMinTemp: null,
          weatherMaxTemp: null,
          weatherDescription: null,
          weatherHumidity: null,
          weatherFeelsLike: null,
          weatherWindSpeed: null,
          hourly_weather: [],
        },
      ],
      dailyWeather: [],
      dailyWeatherDayName: null,
      dailyWeatherDayDate: null,
    };
    this.fetchingData = this.fetchingData.bind(this);
    this.dailyWeather = this.dailyWeather.bind(this);
  }

  // to start fetch data when page loaded
  componentDidMount() {
    this.fetchingData();
    this.fetchingDataSync();
  }

  async fetchingData() {
    try {
      fetch(this.Api)
        .then((res) => res.json())
        .then((json) => {
          // grouping response due to Day date
          var result = json.list.reduce(function (rv, arr) {
            rv[arr.dt_txt.split(" ")[0]] = rv[arr.dt_txt.split(" ")[0]] || [];
            rv[arr.dt_txt.split(" ")[0]].push(arr);

            return rv;
          }, {});
          // formatting fetched data
          var resultfetch = [];
          var count = 1;
          Object.keys(result).map((key) => {
            var days = [
              "Sunday",
              "Monday",
              "Tuesday",
              "Wednesday",
              "Thursday",
              "Friday",
              "Saturday",
            ];
            var d = new Date(result[key][0].dt_txt);
            var dayName = days[d.getDay()];
            if (count <= 5) {
              resultfetch.push({
                dayName: dayName,
                dayDate: result[key][0].dt_txt.split(" ")[0],
                weatherIcon: result[key][0].weather[0]["icon"],
                weatherTemp: result[key][0].main.temp,
                weatherMinTemp: result[key][0].main.temp_min,
                weatherMaxTemp: result[key][0].main.temp_max,
                weatherDescription: result[key][0].weather[0]["description"],
                weatherHumidity: result[key][0].main.humidity,
                weatherFeelsLike: result[key][0].main.feels_like,
                weatherWindSpeed: result[key][0].wind.speed,
                hourly_weather: result[key],
              });
            }
            count++;
          });
          this.setState({
            cityName: json.city.name,
            daysWeather: resultfetch,
          });
        });
    } catch (e) {
      console.log(e);
    }
  }
  async fetchingDataSync() {
    try {
      setInterval(async () => {
        this.fetchingData();
      }, 300000);
    } catch (e) {
      console.log(e);
    }
  }

  // to show daily data of weather
  dailyWeather(list, dayname, daydate) {
    var arr = [];
    list.map((key) => {
      var timeName = key.dt_txt.split(" ")[1].split(":");
      arr.push({
        time: timeName[0] + ":" + timeName[1],
        temp: key.main.temp,
        icon: key.weather[0]["icon"],
      });
    });

    this.setState({
      dailyWeatherDayName: dayname,
      dailyWeatherDayDate: daydate,
      dailyWeather: arr,
    });
  }

  render() {
    return (
      <section className="weather-wrapper py-4 bg-[url('./assets/images/app-bg.jpg')] bg-no-repeat bg-cover bg-bottom min-h-screen text-black">
        <div className="container text-center mx-auto px-3">
          <div className="bg-white bg-opacity-70 p-2 mx-auto max-w-2xl rounded-lg">
            <h2 className="md:text-4xl text-xl font-bold py-3 flex gap-3 items-center justify-center">
              <TiWeatherPartlySunny size={50} /> 5-Day Weather Forecast
            </h2>
            <h4 className="city-name text-4xl py-3 flex gap-3 items-center justify-center">
              <GiModernCity size={30} /> {this.state.cityName}
            </h4>
          </div>
          {/* weather of day per 3 hour */}

          {this.state.dailyWeatherDayName != null && (
            <div className="p-2 mt-4 bg-slate-100 bg-opacity-40 rounded-md max-w-5xl mx-auto">
              <h1 className="bg-slate-100 rounded-md bg-opacity-80 max-w-max mx-auto text-xl ">
                <b className="mx-1"> {this.state.dailyWeatherDayName} </b>
                <span className="mx-1">{this.state.dailyWeatherDayDate}</span>
              </h1>
              <div className="flex flex-wrap justify-center gap-2">
                {this.state.dailyWeather.map((day) => (
                  <div
                    key={day.time}
                    className="dayHourlyWeather text-left border-t-4 border-orange-200 bg-slate-100 mx-1 my-1 p-1 bg-opacity-80 rounded-md min-w-[100px]"
                  >
                    <h3 className="mb-2 flex justify-center items-center">
                      <BiTime size={25} /> {day.time}
                    </h3>
                    <h3 className="text-center font-bold text-lg font-serif">
                      <img
                        className="mx-auto bg-orange-300 rounded-md"
                        src={`http://openweathermap.org/img/wn/${day.icon}.png`}
                      />
                      {day.temp}°C
                    </h3>
                  </div>
                ))}
              </div>
            </div>
          )}
          {/* Weather Data For 5 days */}
          <div className="flex flex-wrap gap-2 items-center justify-center bg-white bg-opacity-30 px-3 py-1 my-5 mx-auto max-w-6xl rounded-lg">
            {this.state.daysWeather.map((day) => (
              <div
                key={day.dayName}
                onClick={() =>
                  this.dailyWeather(
                    day.hourly_weather,
                    day.dayName,
                    day.dayDate
                  )
                }
                className="day-slot p-2 rounded-md border-t-4 my-2 bg-slate-100 bg-opacity-70 text-black w-[210px]"
              >
                <h4 className="day--name text-2xl font-bold mb-2">
                  {day.dayName}
                </h4>
                <h6 className="day--date--time text-base mb-2 font-medium">
                  {day.dayDate}
                </h6>
                <h6 className="day--weather-icon mb-2 ">
                  <img
                    className="mx-auto bg-orange-300 rounded-md"
                    src={`http://openweathermap.org/img/wn/${day.weatherIcon}.png`}
                  />
                </h6>
                <h6 className="day--temp-current text-2xl font-bold font-serif p-1 mx-auto w-max rounded bg-slate-100 bg-opacity-40 mb-2">
                  {day.weatherTemp}°C
                </h6>
                <div className="day--temp-min-max flex justify-between items-start font-medium text-sm py-1 rounded bg-orange-600 bg-opacity-20 mb-2">
                  <span className="day--temp-min mx-1">
                    min: {day.weatherMinTemp}°C
                  </span>

                  <span className="day--temp-max mx-1">
                    max: {day.weatherMaxTemp}°C
                  </span>
                </div>
                <div className="text-left bg-slate-100 bg-opacity-40 p-2 rounded-md">
                  <h6 className="day--weather--state text-lg text-center text-orange-800 font-medium mb-2">
                    {day.weatherDescription}
                  </h6>
                  <div className="day--humidity">
                    <span className="font-medium text-sm">Humidity: </span>
                    <span>{day.weatherHumidity}%</span>
                  </div>
                  <div className="day--wind-speed">
                    <span className="font-medium text-sm">Wind speed: </span>
                    <span>{day.weatherWindSpeed}km/h</span>
                  </div>
                  <div className="day--wind-speed">
                    <span className="font-medium text-sm">Feels like: </span>
                    <span>{day.weatherFeelsLike}</span>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>
    );
  }
}

export default App;
